#include <Windows.h>
#include "FApplication.h"

#ifdef NDEBUG
int WinMain(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPTSTR    lpCmdLine,
    int       cmdShow)
{
    //REDIRECT COUT CERR CLOG
    std::ofstream outCout("runtime_messages.log");
    std::cout.rdbuf(outCout.rdbuf());

    std::ofstream outCerr("runtime_errors.log");
    std::cerr.rdbuf(outCerr.rdbuf());

    std::ofstream outClog("runtime_log.log");
    std::clog.rdbuf(outClog.rdbuf());

    //CREATE APP INSTANCE
    FApplication app;

    //RUN APP
    try
    {
        app.run();
    }
    catch (const std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#else
int main()
{
    FApplication app;

    try
    {
        app.run();
    }
    catch (const std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        std::getchar();

        return EXIT_FAILURE;
    }

    std::getchar();
    return EXIT_SUCCESS;
}
#endif