#include "FWindow.h"
using namespace fEngine;

GLFWmonitor** FWindow::availableMonitors = nullptr;
int FWindow::monitorsCount = 0;

FWindow::FWindow()
{
    glfwWindow = nullptr;

    if (monitorsCount == 0)
        availableMonitors = glfwGetMonitors(&monitorsCount);
}

FWindow::~FWindow()
{
    try
    {
        glfwDestroyWindow(glfwWindow);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Unable to destroy window!" << std::endl << e.what() << std::endl;
    }
}

bool FWindow::ShowWindow(bool fullScreen, uint8_t targetMonitorIndex, bool showBorders, Vector2<uint32_t>& windowSize)
{
    if (glfwWindow != nullptr)
    {
        std::cerr << "Window is already shown!\n";
        return false;
    }

    this->fullScreen = fullScreen;
    this->showBorders = showBorders;

    targetMonitorIndex -= 1;
    if (targetMonitorIndex < monitorsCount && targetMonitorIndex != UINT32_MAX)
    {
        targetMonitor = availableMonitors[targetMonitorIndex];
    }
    else targetMonitor = glfwGetPrimaryMonitor();


    const GLFWvidmode* mode = glfwGetVideoMode(targetMonitor);

    if (windowSize.x == 0 || windowSize.y == 0)
    {
        windowSize.x = mode->width;
        windowSize.y = mode->height;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    if(showBorders) glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
    else glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

    try
    {
        glfwWindow = glfwCreateWindow(windowSize.x, windowSize.y, "FEngine", fullScreen ? targetMonitor : nullptr, nullptr);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Unable to create window of size " << windowSize.ToString() << "!\n" << e.what() << '\n';
        return false;
    }

    int monitorXpos, monitorYpos;
    glfwGetMonitorPos(targetMonitor, &monitorXpos, &monitorYpos);

    glfwSetWindowPos(glfwWindow, mode->width / 2 - windowSize.x / 2 + monitorXpos, mode->height / 2 - windowSize.y / 2 + monitorYpos);

    return true;
}

GLFWwindow * FWindow::GetGLFWWindow()
{
    return glfwWindow;
}