#pragma once
#include <string>

namespace fEngine
{
    template <class T>
    class Vector2
    {
    public:
        Vector2();
        Vector2(T x, T y);

        ~Vector2();

        std::string ToString();

        T x, y;
    };

    template<class T>
    Vector2<T>::Vector2()
    {
        x = 0;
        y = 0;
    }

    template<class T>
    inline Vector2<T>::Vector2(T x, T y)
    {
        this->x = x;
        this->y = y;
    }

    template<class T>
    Vector2<T>::~Vector2()
    {
    }

    template<class T>
    inline std::string Vector2<T>::ToString()
    {
        std::string result = std::to_string(x) + " " + std::to_string(y);
        return result;
    }
}