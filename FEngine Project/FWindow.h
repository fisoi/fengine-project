#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include "Vector2.h"

namespace fEngine
{
    class FWindow
    {
    public:
        FWindow();
        ~FWindow();

        bool ShowWindow(bool fullScreen, uint8_t targetMonitorIndex, bool showBorders, Vector2<uint32_t>& windowSize);
        GLFWwindow * GetGLFWWindow();

    private:
        GLFWwindow * glfwWindow;

        bool fullScreen;
        GLFWmonitor* targetMonitor;

        bool showBorders;

        static GLFWmonitor** availableMonitors;
        static int monitorsCount;
    };
}